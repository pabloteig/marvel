# README #

MarvelAPI:
Tecnologias/ferramentas utilizadas para desenvolvimento da solução:

- Butter Knife - butterknife:8.8.1
- Retrofit - retrofit:2.3.0 / converter-gson:2.3.0
- EventBus - eventbus:3.1.1

Para utilizar o aplicativo apenas é necessário trocar a privateKey e a publicKey na classe Authentication.

privateKey = "xxxxxxxxxxxxxxxxxxx";
publicKey = "xxxxxxxxxxxxxxxxx";