package br.com.pablo.marvelapi.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import br.com.pablo.marvel_api.api.model.Character;
import br.com.pablo.marvelapi.R;
import br.com.pablo.marvelapi.event.MessageEvent;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Pablo on 12/03/2018.
 */

public class CharacterDetailsActivity extends AppCompatActivity {

    @BindView(R.id.character_details_name)
    TextView characterName;

    @BindView(R.id.character_details_image)
    ImageView characterImage;

    @BindView(R.id.character_details_id)
    TextView characterId;

    @BindView(R.id.character_details_description)
    TextView characterDescription;

    @BindView(R.id.character_details_modified)
    TextView characterModified;

    @BindView(R.id.character_details_url)
    TextView characterUrl;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_details);
        ButterKnife.bind(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessageEvent(MessageEvent event) {
        createCharacterDetails(event.getCharacter());
    }

    private void createCharacterDetails(Character character){
        if(character != null){
            try {
                characterName.setText(character.getName());
                String id = String.valueOf(character.getId());
                characterId.setText(id);
                characterDescription.setText(character.getDescription());

                DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault());
                String reportDate = df.format(character.getModified());

                characterModified.setText(reportDate);
                characterUrl.setText(character.getResourceURI());
            }catch (Exception e){
                e.printStackTrace();
            }

            new DownloadImageTask(characterImage).execute(character.getThumbnail().getUrl());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
