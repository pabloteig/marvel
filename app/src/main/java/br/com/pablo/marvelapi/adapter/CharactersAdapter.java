package br.com.pablo.marvelapi.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import br.com.pablo.marvel_api.api.model.Character;
import br.com.pablo.marvelapi.R;
import br.com.pablo.marvelapi.activity.CharacterDetailsActivity;
import br.com.pablo.marvelapi.event.MessageEvent;

/**
 * Created by Pablo on 13/03/2018.
 */

public class CharactersAdapter extends RecyclerView.Adapter<CharactersAdapter.LineHolder> {

    private List<Character> mCharacters;
    private Context mContext;

    // Provide a suitable constructor (depends on the kind of dataset)
    public CharactersAdapter(Context context, List<Character> Characters) {
        this.mContext = context;
        this.mCharacters = Characters;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public LineHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LineHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_characters_adapter_view, parent, false));
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(LineHolder holder, final int position) {
        Character character = mCharacters.get(position);
        holder.title.setText(character.getName());
        holder.moreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMore(position);
            }
        });

    }

    private void showMore(int position){
        Character character = mCharacters.get(position);

        Intent intent = new Intent(mContext, CharacterDetailsActivity.class);
        mContext.startActivity(intent);

        EventBus.getDefault().postSticky(new MessageEvent(character));


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mCharacters != null ? mCharacters.size() : 0;
    }

    public void updateList(List<Character> characters) {
        mCharacters.addAll(characters);
        notifyDataSetChanged();
    }

    public class LineHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public ImageButton moreButton;

        public LineHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.list_characters_name);
            moreButton = itemView.findViewById(R.id.list_characters_details);
        }
    }

}
