package br.com.pablo.marvelapi.event;

import br.com.pablo.marvel_api.api.model.Character;

/**
 * Created by Pablo on 13/03/2018.
 */

public class MessageEvent {

    private Character character;

    public MessageEvent(Character character){
        this.character = character;
    }

    public Character getCharacter() {
        return character;
    }
}
