package br.com.pablo.marvelapi;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.com.pablo.marvel_api.api.config.RetrofitConfig;
import br.com.pablo.marvel_api.api.model.Character;
import br.com.pablo.marvel_api.api.request.Request;
import br.com.pablo.marvel_api.api.response.ServiceResponse;
import br.com.pablo.marvelapi.adapter.CharactersAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity {

    private CharactersAdapter mAdapter;
    private List<Character> mCharacters = new ArrayList<>();
    private static final int limit = 10;
    private boolean loading = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount, offset = 0;

    @BindView(R.id.characters_recycler_view)
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupRecycler();

        start();
    }

    public void start(){
        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(this);
        progressDoalog.setIndeterminate(true);
        progressDoalog.setMessage(getResources().getString(R.string.msg_loadind));
        progressDoalog.show();

        Request request = new Request();
        request.setLimit(limit);
        request.setOffset(offset);

        Call<ServiceResponse<Character>> call = new RetrofitConfig().getCharactersService().listCharacters(
                request.getLimit(),
                request.getOffset(),
                request.getTs(),
                request.getPublicKey(),
                request.getHash(),
                request.getName());
        call.enqueue(new Callback<ServiceResponse<Character>>() {
            @Override
            public void onResponse(Call<ServiceResponse<Character>> call, Response<ServiceResponse<Character>> response) {
                ServiceResponse<Character> result = response.body();
                mCharacters = result.getData().getResults();
                mAdapter.updateList(mCharacters);
                offset = offset + limit;
                loading = true;
                progressDoalog.dismiss();
            }

            @Override
            public void onFailure(Call<ServiceResponse<Character>> call, Throwable t) {
                Log.e("CharactersService   ", "Error:" + t.getMessage());
                t.printStackTrace();
                progressDoalog.dismiss();
            }
        });

    }

    private void setupRecycler() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        mAdapter = new CharactersAdapter(this, mCharacters);
        mRecyclerView.setAdapter(mAdapter);

        // Divider Decoration
        mRecyclerView.addItemDecoration(
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));



        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.v("...", "Last Item Wow !");
                            start();
                        }
                    }
                }

            }

        });

    }

}