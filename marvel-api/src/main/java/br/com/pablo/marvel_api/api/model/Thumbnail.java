package br.com.pablo.marvel_api.api.model;

/**
 * Created by Pablo on 12/03/2018.
 */

public class Thumbnail {
    public String path;
    public String extension;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getUrl(){
        //return this.path + "/portrait_small." + this.extension;
        return this.path + "." + this.extension;
    }
}
