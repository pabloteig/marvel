package br.com.pablo.marvel_api.api.response;

import java.util.List;

/**
 * Created by Pablo on 12/03/2018.
 */

public class DataContainer <E> {

    public int offset;
    public int limit;
    public int total;
    public int count;
    private List<E> results;

    public List<E> getResults() {
        return results;
    }

    public void setResults(List<E> results) {
        this.results = results;
    }
}
