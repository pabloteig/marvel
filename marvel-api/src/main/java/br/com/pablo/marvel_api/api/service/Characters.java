package br.com.pablo.marvel_api.api.service;

import br.com.pablo.marvel_api.api.response.ServiceResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Pablo on 12/03/2018.
 */

public interface Characters {

    @GET("/v1/public/characters")
    Call<ServiceResponse<br.com.pablo.marvel_api.api.model.Character>> listCharacters(@Query("limit") int limit
            , @Query("offset") int offset
            , @Query("ts") String timestamp
            , @Query("apikey") String apikey
            , @Query("hash") String hashSignature
            , @Query("name") String name);

    @GET("/v1/public/characters/{characterid}")
    public void getCharacterWithId(@Path("characterid") int characterId
            , @Query("ts") String timestamp
            , @Query("apikey") String apikey
            , @Query("hash") String hashSignature);
            //, Callback<ServiceResponse<Character>> callback);

}
