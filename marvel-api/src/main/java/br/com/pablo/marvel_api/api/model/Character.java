package br.com.pablo.marvel_api.api.model;

import java.util.Date;
import java.util.List;

/**
 * Created by Pablo on 12/03/2018.
 */

public class Character {

    public int id;
    public String name;
    public String description;
    public Date modified;
    public String resourceURI;
    public List<Url> urls;
    public Thumbnail thumbnail;
    public ItemList comics;
    public ItemList stories;
    public ItemList events;
    public ItemList series;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getResourceURI() {
        return resourceURI;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }

    public List<Url> getUrls() {
        return urls;
    }

    public void setUrls(List<Url> urls) {
        this.urls = urls;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public ItemList getComics() {
        return comics;
    }

    public void setComics(ItemList comics) {
        this.comics = comics;
    }

    public ItemList getStories() {
        return stories;
    }

    public void setStories(ItemList stories) {
        this.stories = stories;
    }

    public ItemList getEvents() {
        return events;
    }

    public void setEvents(ItemList events) {
        this.events = events;
    }

    public ItemList getSeries() {
        return series;
    }

    public void setSeries(ItemList series) {
        this.series = series;
    }
}
