package br.com.pablo.marvel_api.api.request;

import java.util.Date;
import java.util.List;

/**
 * Created by Pablo on 12/03/2018.
 */


public class Request {

    private String name;
    private int limit;
    private int offset;
    private Authentication auth;

    public Request(){
        auth = new Authentication();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getHash(){
        return auth.getHash();
    }

    public String getTs(){
        return String.valueOf(auth.getTs());
    }

    public String getPrivateKey(){
        return Authentication.getPrivateKey();
    }

    public String getPublicKey(){
        return Authentication.getPublicKey();
    }

}
