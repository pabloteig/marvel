package br.com.pablo.marvel_api.api.request;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by Pablo on 12/03/2018.
 */

public class Authentication {

    private static final String privateKey = "07a83020e0c4242c5b5a71fa778070b492c96aff";
    private static final String publicKey = "f729e9be152099f7bcb02fcbaff8621b";
    private long ts;
    private String hash;
    private static Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

    /*
    private static Authentication INSTANCE;

    public static Authentication getInstance() {
        if (INSTANCE == null) INSTANCE = new Authentication();
        return INSTANCE;
    }
    */

    public Authentication(){
        create();
    }

    public void create(){
        this.ts = calendar.getTimeInMillis() / 1000L;
        this.hash = md5(String.valueOf(this.ts) + privateKey + publicKey);
    }

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }

    public String getHash() {
        return hash;
    }

    public static String getPrivateKey() {
        return privateKey;
    }

    public static String getPublicKey() {
        return publicKey;
    }

    private String md5(final String s) {
        final String MD5 = "MD5";
        try {

            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
