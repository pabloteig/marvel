package br.com.pablo.marvel_api.api.model;

import java.util.List;

/**
 * Created by Pablo on 12/03/2018.
 */
public class ItemList {

    public int available;
    public int returned;
    public String collectionURI;
    public List<Item> items;
}
