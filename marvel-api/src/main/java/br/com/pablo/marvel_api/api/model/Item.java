package br.com.pablo.marvel_api.api.model;

/**
 * Created by Pablo on 12/03/2018.
 */
public class Item {

    public String resourceURI;
    public String name;
    public String type;
}
