package br.com.pablo.marvel_api.api.response;


/**
 * Created by Pablo on 12/03/2018.
 */

public class ServiceResponse<E>{
    public int code;
    public String status;
    public String copyright;
    public String attributionText;
    public String attributionHTML;
    public String etag;
    public DataContainer<E> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataContainer<E> getData() {
        return data;
    }

    public void setData(DataContainer<E> data) {
        this.data = data;
    }
}
